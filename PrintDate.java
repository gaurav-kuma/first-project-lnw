/**
*@Author : Gaurav Kumar
Version: 1.0
Date: 13th Sept 2022
Line added
*
*
**/
import java.util.Date;

public class PrintDate
{
    public static void main(String[] args)
    {
        System.out.println("Today's Date and Time: "+new Date());
    }
}
